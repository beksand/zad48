package com.beksand;
//
//Przygotuj program, który wydrukuje na ekran hierarchiczną zależność katalogów: nazwy podkatalogów i plików mają być
// przesunięte w prawo w stosunku do katalogu nadrzędnego. Przygotuj wersję rekurencyjną i iteracyjną.
//        Rekurencyjna wersja:
//        func drzewo (wcięcie, plik)
//        wyświetl wcięcie
//        wyświetl nazwę pliku
//        jeżeli aktualny plik jest folderem
//        wywołaj rekurencyjnie aktualną funkcję jako parametry dając wcięcie powiększone o spację oraz listę plików w
// tym folderze
//        Wywołaj funkcję dla folderu z projektami.
//        * hint: sprawdzenie czy plik jest katalogiem:
//        File f = new File(path);
//        f.isDirectory()
//        * hint: pobranie plików w danym katalogu:
//        File f = new File(path);
//        f.listFiles();

import java.io.File;

public class Main {

	public static void main(String[] args) {
		File file1 = new File("file1.txt");
		File file2 = new File("file2.txt");
		File folder = new File("/home/beks/IdeaProjects/Zad48/src/com/beksand/Folder");


		if (folder.isDirectory()) {
			System.out.println("directory");
		} else {
			System.out.println("no directory");
		}
		System.out.println(folder.getPath());
		drzewo(folder, "");
	}

	public static void drzewo(File f, String sp) {
		System.out.println(sp + f.getName());
		if (f.isDirectory()) {
			File[] arrf = f.listFiles();
			for (int i = 0; i < arrf.length; i++) {

					drzewo(arrf[i], sp + "-");
			}


		}

	}
}